﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //let me make get a string
            string temp_string = "hello";

            //now I will try to convert it into something that is not allowed
            //since the string is not an actual number that can be successfully converted, this will raise an exception
            //remove the commenting thing to see the exception
            //int temp_int = Convert.ToInt32(temp_string);

            //here I wil do the exact same thing but use the exception catching and handling stuff
            //this means, the code should try do what is inside the try block
            try
            {
                int temp_int_2 = Convert.ToInt32(temp_string);
            }
            catch(Exception e) //if something went wrong, enter this block. collect information about the exception
            {
                //I can use an exception object e to get more details about the exception itself. 
                Console.WriteLine(" {0} ", e.Message);
                Console.WriteLine(" {0} ", e.Source);
                Console.WriteLine(" {0} ", e.StackTrace);
                Console.WriteLine(" {0} ", e.HResult);

            }
            finally  //this block code will execute no matter what happened in try or catch blocks of code
            {
                Console.WriteLine("This portion of try sequence will always work, no matter what happened before it ");
            }

            //this is the same as above. This time, instead of catching any and all exception with a generic exception
            //I will try and catch individual exceptions
            //this allows you to show different types of messages depending on what value was entered.

            Console.WriteLine("Enter a number. ");
            Console.WriteLine("If you enter a word, you will get a word related error");
            Console.WriteLine("If you enter a number greater than 2,147,483,647  you will get capacity exceeded error");
            temp_string = Console.ReadLine();

            try
            {
                int temp3 = Convert.ToInt32(temp_string);
                Console.WriteLine("Looks like you entered a proper number i.e. {0}", temp3);

                //you can also prevent finally from happeneing by using the following
                //suppose the entered number is 100
                if(temp3 == 100)
                {
                    Environment.FailFast("looks like you want to avoid finally from running");
                }
            }
            catch(FormatException e)
            {
                Console.WriteLine("you did not enter a number. sorry.");
            }
            catch(OverflowException e)
            {
                Console.WriteLine("the number you entered is too large for int type to handle");
            }
            finally
            {
                Console.WriteLine("Its all done");
            }


            //preventing the console from dissapearing
            Console.ReadLine();
        }
    }
}
